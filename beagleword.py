#!/usr/bin/python3
import sys
import nltk
import spacy as sp


text = sys.stdin.read()
nlp = sp.load('en_core_web_md')

words = []

sentences = nltk.sent_tokenize(text)
for sentence in sentences:
    parsed = nlp(sentence)
    for token in parsed:
        phrase = ""
        if token.is_alpha:
            if token.dep_ == "prt" and token.head.pos_ == "VERB":
                phrase = token.head.orth_ + " " + token.orth_
            else:
                phrase = token.lemma_
            if phrase not in words:
                words.append(phrase)

for word in words:
    print(word)
