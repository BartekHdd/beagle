#!/usr/bin/python3

import sys
import re
import mimetypes

def main():
    if len(sys.argv) != 2:
        print("Usage: beagle <file>")
        sys.exit(1)
    file_ext = re.findall(r'\.[^\.]+$', sys.argv[1])[0]
    if file_ext == []:
        print("Usage: beagle <file.{extension here, please}>")
        sys.exit(1)
    if file_ext == ".txt":
        parser = TxtParser(sys.argv[1])
    elif file_ext == ".srt":
        parser = SrtParser(sys.argv[1])
    elif (mimetypes.guess_type(sys.argv[1])[0].split("/")[0] == "audio" or
          mimetypes.guess_type(sys.argv[1])[0].split("/")[0] == "video"):
        parser = AudioParser(sys.argv[1])
    else:
        print("File type not supported :,(")
    print(parser.getText())

class TxtParser:
    def __init__(self, file_path):
        file = open(file_path, "r")
        self.text = file.read()
        file.close()
    def getText(self):
        return self.text

class AudioParser:
    def __init__(self, file_path):
        import whisper
        self.text = whisper.load_model("tiny").transcribe(file_path)["text"]
    def getText(self):
        return self.text
class SrtParser:
    def __init__(self, file_path):
        self.text = ""
        self.load_raw_text(file_path)
        self.remove_markups()
    def load_raw_text(self, file_path):
            lines = open(file_path, "r").readlines()
            for line in lines:
                line = line.rstrip('\n')
                if re.search('^[0-9]+$', line) is None and re.search('^[0-9]{2}:[0-9]{2}:[0-9]{2}', line) is None and re.search('^$', line) is None:
                    self.text += ' ' + line
    def remove_markups(self):
        self.text = re.sub(r'<[^<>]*>', ' ', self.text)
    def getText(self):
        return self.text

if __name__ == '__main__':
    main()
