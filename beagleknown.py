#!/usr/bin/python3
import sys
import time
import os

BLACKLIST_PATH = sys.argv[1]

words_input = sys.stdin.read().split("\n")

known = open(BLACKLIST_PATH, "r").read().split("\n")

words = [x for x in words_input if x.lower() not in known]

with open("temporary.txt", "w") as temp:
    temp.write("\n".join(words))
os.system("xdg-open temporary.txt")

while True:
    time.sleep(5)
    with open("temporary.txt", "r") as temp:
        edited = temp.read().split("\n")
    if edited != words:
        break

if os.path.exists("temporary.txt"):
    os.remove("temporary.txt")

unexpected_but_known = [x for x in words if x not in edited]

with open(BLACKLIST_PATH, "a") as known:
    known.write("\n".join(unexpected_but_known)+"\n")

print("\n".join([w for w in words if w not in unexpected_but_known]))
