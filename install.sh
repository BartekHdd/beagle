#!/usr/bin/bash

#python -m spacy download en_core_web_md

cp ./beagle.py ~/.local/bin/beagle
cp ./beagleword.py ~/.local/bin/beagleword
cp ./beagleknown.py ~/.local/bin/beagleknown
cp ./beagletrainer.py ~/.local/bin/beagletrainer

chmod +x ~/.local/bin/beagle 
chmod +x ~/.local/bin/beagleword
chmod +x ~/.local/bin/beagleknown
chmod +x ~/.local/bin/beagletrainer