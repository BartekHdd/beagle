My personal english language learning tool. It's designed to load .srt (film subtitles) files and extract words I likely don't know. 

It probably won't work on non-unix systems :(
_Tested on Zorin OS 16.1_

Installation:
```bash
pip install -r requirements.txt
python -m spacy download en_core_web_md
sh install.sh
```

Example use:
```bash
beagle mooovie.srt | beagleword | beagleknown ultimate.black > words.txt
```
